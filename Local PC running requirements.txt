############ Requirements for running mathgame-4 application in local PC

This application use virtual environment 
It is created folder "python_local_packages" for the virtuial enviroment
The packages for this application are located in python_local_packages/Lib

The virtual enviroment python_local_packages folder is at the root of porject folder mathgame-4, as well as folder src which contains the application code. 
Thus virtual enviroment and code are located in independent folders  


############ Pre-requisites
Have installed 
Python 
VSC

############ Check 
cmd
python --version


############ At the root mathgame-4 folder create the virtual enviroment (mathgame-4/)
py -m venv python_local_packages 


############ Go inside folder src where is located the app.py file
cd src 
ls

############ Activate the virtual enviroment at the folder src where  the code appy is located (src/)
..\\python_local_packages\Scripts\activate



############ At the virtual enviroment check the current python packages 
pip list
pip freeze


############ At the virtual enviroment Upgrade pip package
python -m pip install --upgrade pip


############ At the virtual enviroment Install the required python packages for this application 
pip install flask
pip install frozen-Flask


############ At the virtual enviroment Check the packages installed
pip list 
pip freeze

############ At the virtual enviroment Run the application 
set FLASK_APP=app.py
flask run


############ On web browser paste
http://127.0.0.1:5000/
or
localhost:5000




############ Application checks and running logs 

PS C:\Users\T877187\proyects\Training\mathgame-4> python --version
Python 3.10.1
PS C:\Users\T877187\proyects\Training\mathgame-4> ls


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----        12/28/2021   6:34 PM                python_local_packages
d-----        12/29/2021   3:02 PM                src
-a----        12/29/2021   5:33 AM             88 .gitignore
-a----        12/29/2021   3:23 PM           3697 Local PC running requirements.txt
PS C:\Users\T877187\proyects\Training\mathgame-4\src> ls


    Directory: C:\Users\T877187\proyects\Training\mathgame-4\src


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----        12/28/2021  10:32 AM                frontend
d-----        12/28/2021   7:01 PM                static
d-----        12/28/2021  10:49 AM                __pycache__
-a----        12/28/2021  10:38 AM           2414 app.py


PS C:\Users\T877187\proyects\Training\mathgame-4\src> ..\\python_local_packages\Scripts\activate
(python_local_packages) PS C:\Users\T877187\proyects\Training\mathgame-4\src> 
(python_local_packages) PS C:\Users\T877187\proyects\Training\mathgame-4\src> 
(python_local_packages) PS C:\Users\T877187\proyects\Training\mathgame-4\src> pip list
Package      Version
------------ -------
click        8.0.3
colorama     0.4.4
Flask        2.0.2
Frozen-Flask 0.18
itsdangerous 2.0.1
Jinja2       3.0.3
MarkupSafe   2.0.1
pip          21.3.1
setuptools   58.1.0
Werkzeug     2.0.2
(python_local_packages) PS C:\Users\T877187\proyects\Training\mathgame-4\src> 
(python_local_packages) PS C:\Users\T877187\proyects\Training\mathgame-4\src>
(python_local_packages) PS C:\Users\T877187\proyects\Training\mathgame-4\src> pip freeze
click==8.0.3
colorama==0.4.4
Flask==2.0.2
Frozen-Flask==0.18
itsdangerous==2.0.1
Jinja2==3.0.3
MarkupSafe==2.0.1
Werkzeug==2.0.2
(python_local_packages) PS C:\Users\T877187\proyects\Training\mathgame-4\src> 
(python_local_packages) PS C:\Users\T877187\proyects\Training\mathgame-4\src>
(python_local_packages) PS C:\Users\T877187\proyects\Training\mathgame-4\src> set FLASK_APP=app.py
(python_local_packages) PS C:\Users\T877187\proyects\Training\mathgame-4\src> flask run
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
Running template in the following path: ./frontend
Running static files in the following path: C:\Users\T877187\proyects\Training\mathgame-4\src\static
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
